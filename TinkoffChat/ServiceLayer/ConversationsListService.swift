//
//  ConversationsListService.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 29.10.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation
import UIKit

protocol IConversationsListService: class {
	weak var conversationsListDataProvider: ConversationsListDataProvider? { get set }
	func createConversationsListDataProvider(with tableView: UITableView, coreDataStack: CoreDataStack) -> ConversationsListDataProvider?
}

// MARK: - IConversationsListService

class ConversationsListService: IConversationsListService {
	
	var conversationsListDataProvider: ConversationsListDataProvider?
	let manager: ICommunicatorManager
	
	init(manager: ICommunicatorManager) {
		self.manager = manager
	}
	
	func createConversationsListDataProvider(with tableView: UITableView, coreDataStack: CoreDataStack) -> ConversationsListDataProvider? {
		conversationsListDataProvider = ConversationsListDataProvider(tableView: tableView, coreDataStack: coreDataStack)
		return conversationsListDataProvider
	}
}
