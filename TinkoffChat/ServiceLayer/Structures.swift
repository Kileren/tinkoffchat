//
//  Structures.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 31.10.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation
import MultipeerConnectivity

struct MessageStruct {
	var text: String
	var type: String
	var date: Date
	var eventType: String
	var messageID: String
}

struct ProfileInfo {
	var image: UIImage
	var name: String
	var aboutInfo: String
}

struct UserInfo {
	var name: String
	var userID: String
	var isOnline: Bool
}
