//
//  ImagesService.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 16.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation

protocol IImagesService: class {
	func loadImages(request: String?, completionHandler: @escaping (ImageApiModel?, String?) -> Void)
}

class ImagesService: IImagesService {
	
	let requestSender: IRequestSender
	
	init(requestSender: IRequestSender) {
		self.requestSender = requestSender
	}
	
	func loadImages(request: String?, completionHandler: @escaping (ImageApiModel?, String?) -> Void) {
		
		var requestConfig: RequestConfig<ImageApiModel>
		if let request = request {
			requestConfig = RequestsFactory.PixabayRequests.imagesConfigForSpecificRequest(request: request)
		} else {
			requestConfig = RequestsFactory.PixabayRequests.imagesConfig()
		}
		
		requestSender.send(config: requestConfig) { (result: Result<ImageApiModel>) in
			switch result {
			case .success(let images):
				completionHandler(images, nil)
			case .error(let error):
				completionHandler(nil, error)
			}
		}
	}
}
