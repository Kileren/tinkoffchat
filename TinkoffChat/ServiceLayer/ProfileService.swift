//
//  ProfileService.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 05.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation

protocol IProfileService: class {
	var delegate: IProfileServiceDelegate? { get set }
	func saveProfileInfo(info: ProfileInfo)
	func retrieveProfileInfo()
}

protocol IProfileServiceDelegate: class {
	func gotProfileInfo(info: ProfileInfo)
}

class ProfileService: IProfileService {
	
	var delegate: IProfileServiceDelegate?
	let manager: StorageManager
	
	init(manager: StorageManager) {
		self.manager = manager
	}
	
	func saveProfileInfo(info: ProfileInfo) {
		manager.saveProfileInfo(info: info)
	}
	
	func retrieveProfileInfo() {
		let info = manager.retrieveProfileInfo()
		if let profileInfo = info {
			delegate?.gotProfileInfo(info: profileInfo)
		}
	}
}
