//
//  ConversationService.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 29.10.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation
import UIKit

protocol IConversationService: class {
	weak var conversationDataProvider: ConversationDataProvider? { get set }
	weak var conversationModelDelegate: IConversationModelDelegate? { get set }
	func sendMessage(message: MessageStruct, to user: String, completionHandler: @escaping (Bool, Error?) -> Void)
	func deleteMessage(withID id: String)
	func changeLastMessageForConversation(withID id: String, messageID: String)
	func createConversationDataProvider(with tableView: UITableView, coreDataStack: CoreDataStack, conversationID: String) -> ConversationDataProvider?
	func messagesInConversationHasBeenRead(id: String)
}

protocol IConversationModelDelegate: class {
	func didLostUser(userID: String)
	func didFoundUser(withID userID: String, name: String)
}

// MARK: - IConversationService

class ConversationService: IConversationService {
	
	var conversationDataProvider: ConversationDataProvider?
	var conversationModelDelegate: IConversationModelDelegate?
	let manager: ICommunicatorDialogManager
	
	init(manager: ICommunicatorDialogManager) {
		self.manager = manager
		manager.conversationDelegate = self
	}
	
	func sendMessage(message: MessageStruct, to user: String, completionHandler: @escaping (Bool, Error?) -> Void) {
		manager.sendMessage(message: message, to: user, completionHandler: completionHandler)
	}
	
	func deleteMessage(withID id: String) {
		guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
		let storageManager = appDelegate.rootAssembly.storageManager
		storageManager.deleteMessage(withID: id)
	}
	
	func changeLastMessageForConversation(withID id: String, messageID: String) {
		guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
		let storageManager = appDelegate.rootAssembly.storageManager
		storageManager.changeLastMessageForConversation(withID: id, messageID: messageID)
	}
	
	func createConversationDataProvider(with tableView: UITableView, coreDataStack: CoreDataStack, conversationID: String) -> ConversationDataProvider? {
		conversationDataProvider = ConversationDataProvider(tableView: tableView, coreDataStack: coreDataStack, conversationID: conversationID)
		return conversationDataProvider
	}
	
	func messagesInConversationHasBeenRead(id: String) {
		guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
		let storageManager = appDelegate.rootAssembly.storageManager
		storageManager.messagesInConversationHasBeenRead(conversationID: id)
	}
}

extension ConversationService: ICommunicationDialogManagerDelegate {
	
	func didLostUser(userID: String) {
		conversationModelDelegate?.didLostUser(userID: userID)
	}
	
	func didFoundUser(withID userID: String, name: String) {
		conversationModelDelegate?.didFoundUser(withID: userID, name: name)
	}
}
