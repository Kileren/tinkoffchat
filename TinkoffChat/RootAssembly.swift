//
//  RootAssembly.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 29.10.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation

class RootAssembly {
	
	// CONVERSATIONS
	
	var communicationManager: CommunicationManager
	var conversationsListService: IConversationsListService
	var conversationService: IConversationService
	
	// PROFILE
	
	var storageManager: StorageManager
	var profileService: ProfileService
	
	// IMAGES
	
	var requestSender: RequestSender
	var imagesService: ImagesService
	
	init() {
		communicationManager = CommunicationManager()
		conversationsListService = ConversationsListService(manager: communicationManager)
		conversationService = ConversationService(manager: communicationManager)
		
		storageManager = StorageManager()
		profileService = ProfileService(manager: storageManager)
		
		requestSender = RequestSender()
		imagesService = ImagesService(requestSender: requestSender)
	}
}
