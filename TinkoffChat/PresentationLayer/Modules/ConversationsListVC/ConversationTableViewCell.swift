//
//  ConversationTableViewCell.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 06.10.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import UIKit

protocol ConversationCellConfiguration: class {
	var name: String? { get set }
	var message: String? { get set }
	var date: Date? { get set }
	var online: Bool { get set }
	var hasUnreadMessages: Bool { get set }
}

class ConversationTableViewCell: UITableViewCell, ConversationCellConfiguration {
	
	var name: String? {
		didSet {
			if name == nil {
				nameLabel.text = "-"
			} else {
				nameLabel.text = name
			}
		}
	}
	
	var message: String? {
		didSet {
			if message == nil {
				messageLabel.font = UIFont.init(name: "Avenir-LightOblique", size: 15)
				messageLabel.text = "No messages yet"
			} else {
				messageLabel.text = message
			}
		}
	}
	
	var date: Date? {
		didSet {
			if date == nil {
				dateLabel.text = ""
			} else {
				let timeIntervalForADay: TimeInterval = 86400
				let dateFormatter = DateFormatter()
				if date!.timeIntervalSince1970 < Date.init().timeIntervalSince1970 - timeIntervalForADay {
					dateFormatter.dateFormat = "dd MMM"
					dateLabel.text = dateFormatter.string(from: date!)
				} else {
					dateFormatter.dateFormat = "HH:mm"
					dateLabel.text = dateFormatter.string(from: date!)
				}
			}
		}
	}
    var online: Bool = false {
        didSet {
            if online {
                self.backgroundColor = #colorLiteral(red: 0.9764705896, green: 0.9301822492, blue: 0.7031375846, alpha: 1)
            } else {
                self.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
        }
    }
    var hasUnreadMessages: Bool = false {
        didSet {
            if hasUnreadMessages && self.message != nil {
                self.messageLabel.font = UIFont.init(name: "AppleSDGothicNeo-Bold", size: 15)
            }
            if !hasUnreadMessages && self.message != nil {
                self.messageLabel.font = UIFont.init(name: "AppleSDGothicNeo-Thin", size: 15)
            }
        }
    }
    
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var messageLabel: UILabel!
    
	func configure(with conversation: Conversation) {
		if let user = conversation.users {
			self.name = user.name
		}
		
		self.message = conversation.lastMessage?.text
		self.date = conversation.lastMessage?.date
		self.online = conversation.isOnline
		self.hasUnreadMessages = conversation.hasUnreadMessage
    }
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

