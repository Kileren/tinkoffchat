//
//  ConversationsListViewController.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 05.10.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import UIKit

class ConversationsListViewController: UIViewController {
	
	// MARK: - Declaration
	
	@IBOutlet weak var conversationsListTable: UITableView!
	var model: IConversationsListModel?
	var dataProvider: ConversationsListDataProvider?
	var logoEmitter: ILogoEmitter?
	var logoEmitterForNavigationController: ILogoEmitter?
	
	// MARK: - Loading
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		configureConversationsListTable()
		makeLogoEmitters()
		
		guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
		model = getModel(appDelegate: appDelegate)
		dataProvider = getDataProvider(appDelegate: appDelegate)
    }
	
	func configureConversationsListTable() {
		conversationsListTable.delegate = self
		conversationsListTable.dataSource = self
		conversationsListTable.tableFooterView = UIView(frame: CGRect.zero)
	}
	
	func makeLogoEmitters() {
		logoEmitter = LogoEmitter(view: self.view)
		if let navigationView = self.navigationController?.view {
			logoEmitterForNavigationController = LogoEmitter(view: navigationView)
		}
	}
	
	func getModel(appDelegate: AppDelegate) -> ConversationsListModel {
		return ConversationsListModel(conversationListService: appDelegate.rootAssembly.conversationsListService)
	}
	
	func getDataProvider(appDelegate: AppDelegate) -> ConversationsListDataProvider? {
		guard let coreDataStack = appDelegate.rootAssembly.storageManager.coreDataStack else { return nil }
		return model?.createConversationsListDataProvider(with: conversationsListTable, coreDataStack: coreDataStack)
	}
	
	// MARK: - Additional Functions
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		guard segue.identifier == "showMessagesSegue",
			let indexPath = conversationsListTable.indexPathForSelectedRow,
			let destinationVC = segue.destination as? ConversationViewController,
			let frc = dataProvider?.frc else { return }
		
		let conversation = frc.object(at: indexPath)
		guard let user = conversation.users else { return }
		
		destinationVC.titleOfConversation = user.name
		destinationVC.isOnline = user.isOnline
		if let userID = user.userID {
			destinationVC.conversationID = userID
		}
	}
}

// MARK: - Table View

extension ConversationsListViewController: UITableViewDelegate, UITableViewDataSource {
	
	func numberOfSections(in tableView: UITableView) -> Int {
        guard let frc = dataProvider?.frc, let sectionCount = frc.sections?.count else { return 0 }
		return sectionCount
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let frc = dataProvider?.frc, let sections = frc.sections else { return 0 }
        return sections[section].numberOfObjects
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellForConversation") as! ConversationTableViewCell
        if let conversation = dataProvider?.frc?.object(at: indexPath) {
            cell.configure(with: conversation)
        }
        return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 50
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 80
	}
	
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		guard let frc = dataProvider?.frc,
			let sections = frc.sections else { return nil }
		let section = sections[section]
		
		return section.name == "0" ? "Offline" : "Online"
	}
}
