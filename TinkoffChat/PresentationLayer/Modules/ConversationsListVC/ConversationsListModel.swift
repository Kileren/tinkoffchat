//
//  ConversationsListModel.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 29.10.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation
import UIKit

protocol IConversationsListModel: class {
	func createConversationsListDataProvider(with tableView: UITableView, coreDataStack: CoreDataStack) -> ConversationsListDataProvider?
}

class ConversationsListModel: IConversationsListModel {
	
	let conversationListService: IConversationsListService
	
	init(conversationListService: IConversationsListService) {
		self.conversationListService = conversationListService
	}
	
	// MARK: - IConversationsListModel
	
	func createConversationsListDataProvider(with tableView: UITableView, coreDataStack: CoreDataStack) -> ConversationsListDataProvider? {
		let dataProvider = conversationListService.createConversationsListDataProvider(with: tableView, coreDataStack: coreDataStack)
		return dataProvider
	}
}
