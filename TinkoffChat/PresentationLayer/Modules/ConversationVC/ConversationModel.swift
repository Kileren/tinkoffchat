//
//  ConversationModel.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 29.10.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation
import UIKit

protocol IConversationModel: class {
	weak var delegate: IConversationConnection? { get set }
	func sendMessage(message: MessageStruct, to user: String)
	func deleteMessage(withID id: String)
	func changeLastMessageForConversation(withID id: String, messageID: String)
	func createConversationDataProvider(with tableView: UITableView, coreDataStack: CoreDataStack, conversationID: String) -> ConversationDataProvider?
	func messagesInConversationHasBeenRead(id: String)
}

protocol IConversationConnection: class {
	func didResumeConnecton()
	func didLostConnection()
}

class ConversationModel: IConversationModel {
	
	let conversationService: IConversationService
	var userID: String
	var delegate: IConversationConnection?
	
	init(conversationService: IConversationService, forUserID userID: String) {
		self.conversationService = conversationService
		self.userID = userID
		self.conversationService.conversationModelDelegate = self
	}
	
	func sendMessage(message: MessageStruct, to user: String) {
		conversationService.sendMessage(message: message, to: user) { (isSuccess, error) in
			guard error == nil else {
				print(error!.localizedDescription)
				return
			}
			
			if isSuccess {
				print("Successfully send")
			} else {
				print("Can't send message")
			}
		}
	}
	
	func deleteMessage(withID id: String) {
		conversationService.deleteMessage(withID: id)
	}
	
	func changeLastMessageForConversation(withID id: String, messageID: String) {
		conversationService.changeLastMessageForConversation(withID: id, messageID: messageID)
	}
	
	func createConversationDataProvider(with tableView: UITableView, coreDataStack: CoreDataStack, conversationID: String) -> ConversationDataProvider? {
		return conversationService.createConversationDataProvider(with: tableView, coreDataStack: coreDataStack, conversationID: conversationID)
	}
	
	func messagesInConversationHasBeenRead(id: String) {
		conversationService.messagesInConversationHasBeenRead(id: id)
	}
	
	// MARK: - Additional Functions
	
	func createMessage(fromText text: String) -> [String: String] {
		let message: [String: String] = ["eventType": "TextMessage",
										 "messageID": self.generateMessageID(),
										 "text": text]
		return message
	}
	
	func generateMessageID() -> String {
		let string = "\(arc4random_uniform(UINT32_MAX)) + \(Date.timeIntervalSinceReferenceDate) + \(arc4random_uniform(UINT32_MAX))".data(using: .utf8)?.base64EncodedString()
		return string!
	}
}

extension ConversationModel: IConversationModelDelegate {
	
	func didLostUser(userID: String) {
		if self.userID == userID {
			delegate?.didLostConnection()
		}
	}
	
	func didFoundUser(withID userID: String, name: String) {
		if self.userID == userID {
			delegate?.didResumeConnecton()
		}
	}
}
