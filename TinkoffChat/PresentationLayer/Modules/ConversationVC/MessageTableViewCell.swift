//
//  MessageTableViewCell.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 08.10.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {
	
	var dateForIncomeMessage: Date? {
		didSet {
			if dateForIncomeMessage == nil {
				dateForIncomeMessageLabel.text = ""
			} else {
				let timeIntervalForADay: TimeInterval = 86400
				let dateFormatter = DateFormatter()
				if dateForIncomeMessage!.timeIntervalSince1970 < Date.init().timeIntervalSince1970 - timeIntervalForADay {
					dateFormatter.dateFormat = "dd MMM"
					dateForIncomeMessageLabel.text = dateFormatter.string(from: dateForIncomeMessage!)
				} else {
					dateFormatter.dateFormat = "HH:mm"
					dateForIncomeMessageLabel.text = dateFormatter.string(from: dateForIncomeMessage!)
				}
			}
		}
	}
	
	var dateForOutcomeMessage: Date? {
		didSet {
			if dateForOutcomeMessage == nil {
				dateForOutcomeMessageLabel.text = ""
			} else {
				let timeIntervalForADay: TimeInterval = 86400
				let dateFormatter = DateFormatter()
				if dateForOutcomeMessage!.timeIntervalSince1970 < Date.init().timeIntervalSince1970 - timeIntervalForADay {
					dateFormatter.dateFormat = "dd MMM"
					dateForOutcomeMessageLabel.text = dateFormatter.string(from: dateForOutcomeMessage!)
				} else {
					dateFormatter.dateFormat = "HH:mm"
					dateForOutcomeMessageLabel.text = dateFormatter.string(from: dateForOutcomeMessage!)
				}
			}
		}
	}
	
	@IBOutlet weak var incomeMessageLabel: UILabel!
	@IBOutlet weak var outcomeMessageLabel: UILabel!
	
	@IBOutlet weak var dateForIncomeMessageLabel: UILabel!
	@IBOutlet weak var dateForOutcomeMessageLabel: UILabel!
	
	var messageID: String?
	
	func configure(with message: Message, amISender: Bool) {
		
		if amISender {
			outcomeMessageLabel.text = message.text
			dateForOutcomeMessage = message.date
		} else {
			incomeMessageLabel.text = message.text
			dateForIncomeMessage = message.date
		}
		
		if let messageID = message.messageID {
			self.messageID = messageID
		}
	}
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
