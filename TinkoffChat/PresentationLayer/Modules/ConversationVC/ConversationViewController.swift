//
//  ConversationViewController.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 08.10.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import UIKit

class ConversationViewController: UIViewController {
	
	// MARK: - Declaration
	
	@IBOutlet weak var messagesTable: UITableView!
	
	@IBOutlet weak var keyboardView: UIView!
	@IBOutlet weak var newMessageTextField: UITextField!
	@IBOutlet weak var sendBtn: UIButton!
	
	var conversationID = ""
	var isOnline: Bool = true
	
	var bottomConstraintForKeyboardView: NSLayoutConstraint?
	var bottomConstraintForMessageTable: NSLayoutConstraint?
	
	var titleOfConversation: String? = nil
	var titleLabel: UILabel? = nil
	var hadBtnCharacters = false
	
	var model: IConversationModel?
	var dataProvider: ConversationDataProvider?
	var logoEmitter: ILogoEmitter?
	var logoEmitterForNavigationController: ILogoEmitter?
	
	let decoder = Decoder()
	
	// MARK: - Loading

    override func viewDidLoad() {
        super.viewDidLoad()
		
		makeTitle()
		configureMessageTable()
		addTapForKeyboardHiding()
		addAdditionalConstraints()
		registerForNotification()
		
		self.logoEmitter = LogoEmitter(view: self.view)
		if let navigationView = self.navigationController?.view {
			logoEmitterForNavigationController = LogoEmitter(view: navigationView)
		}
		
		guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
		self.model = getModel(with: appDelegate, forUserID: conversationID)
		self.model?.delegate = self
		self.dataProvider = getDataProvider(with: appDelegate)
    }
	
	override func viewDidLayoutSubviews() {
		messagesTable.scrollToLastCell(animated: false, afterNewMessage: false, section: 0)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(true)
		model?.messagesInConversationHasBeenRead(id: conversationID)
	}
	
	func makeTitle() {
		
		let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width * 2 / 3, height: 40))
		label.text = titleOfConversation
		label.textAlignment = .center
		if isOnline { label.textColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1) } else { label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) }
		self.titleLabel = label
		if isOnline {
			let size = sizeAnimation(fromValue: 1, toValue: 1.1, duration: 0, keyValue: nil)
			self.titleLabel?.layer.add(size, forKey: nil)
		}
		
		let viewForTitle = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width * 2 / 3, height: 40))
		if let titleLabel = titleLabel {
			viewForTitle.addSubview(titleLabel)
		}
		navigationItem.titleView = viewForTitle
	}
	
	func configureMessageTable() {
		messagesTable.delegate = self
		messagesTable.dataSource = self
		messagesTable.rowHeight = UITableViewAutomaticDimension
		messagesTable.tableFooterView = UIView(frame: CGRect.zero)
	}
	
	func addTapForKeyboardHiding() {
		let tap = UITapGestureRecognizer(target: self, action: #selector(self.hideKBOnTap))
		tap.delegate = self
		self.messagesTable.addGestureRecognizer(tap)
	}
	
	func addAdditionalConstraints() {
		bottomConstraintForKeyboardView = NSLayoutConstraint(item: keyboardView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
		bottomConstraintForMessageTable = NSLayoutConstraint(item: messagesTable, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: -40)
		view.addConstraint(bottomConstraintForKeyboardView!)
		view.addConstraint(bottomConstraintForMessageTable!)
	}
	
	func getModel(with appDelegate: AppDelegate, forUserID userID: String) -> IConversationModel {
		return ConversationModel(conversationService: appDelegate.rootAssembly.conversationService, forUserID: userID)
	}
	
	func getDataProvider(with appDelegate: AppDelegate) -> ConversationDataProvider? {
		guard let coreDataStack = appDelegate.rootAssembly.storageManager.coreDataStack else { return nil }
		return self.model?.createConversationDataProvider(with: messagesTable, coreDataStack: coreDataStack, conversationID: conversationID)
	}
	
	// MARK: - Additional Functions
	
	@IBAction func textFieldEditingChanged(_ sender: UITextField) {
		
		guard isOnline else { return }
		
		if sender.text != "" && !hadBtnCharacters {
			textFieldChanged(state: true)
		} else if sender.text == "" && hadBtnCharacters {
			textFieldChanged(state: false)
		}
	}
	
	@IBAction func sendBtnAction(_ sender: UIButton) {
		guard let text = newMessageTextField.text else { return }
		let message = decoder.decode(text: text)
		model?.sendMessage(message: message, to: conversationID)
		
		self.view.endEditing(true)
		newMessageTextField.text = ""
		hadBtnCharacters = false
		let animation = sizeAnimation(fromValue: 1, toValue: 1.15, duration: 0.5, keyValue: "sizeBigger")
		sendBtn.layer.add(animation, forKey: nil)
		sendBtn.isEnabled = false
	}
	
	func textFieldChanged(state: Bool) {
		let sizeForSendBtn = sizeAnimation(fromValue: 1, toValue: 1.15, duration: 0.5, keyValue: "sizeBigger")
		sendBtn.layer.add(sizeForSendBtn, forKey: nil)
		sendBtn.isEnabled = state
		hadBtnCharacters = state
	}
	
	// MARK: - Notifications (Keyboard, value changes)
	
	func registerForNotification() {
		NotificationCenter.default.addObserver(self, selector: #selector(self.kbWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.kbWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
	}
	
	func removeNotifications() {
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
	}
	
	deinit {
		removeNotifications()
	}
	
	@objc func kbWillShow(_ notification: Notification) {
		let userInfo = notification.userInfo
		let kbFrameSize = (userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
		
		bottomConstraintForKeyboardView!.constant = -kbFrameSize.height
		
		var cellsHeight: CGFloat = 0
		for cell in messagesTable.visibleCells {
			cellsHeight += cell.bounds.height
		}

		let differentHeight = view.bounds.height - (kbFrameSize.height + newMessageTextField.bounds.height + 64 + cellsHeight)
		print(differentHeight)

		if differentHeight < 0 {
			bottomConstraintForMessageTable!.constant = -kbFrameSize.height - 40
		}
		
		UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
			self.view.layoutIfNeeded()
			self.messagesTable.scrollToLastCell(animated: false, afterNewMessage: false, section: 0)
		}, completion: nil)
	}
	
	@objc func kbWillHide() {
		bottomConstraintForKeyboardView!.constant = 0
		bottomConstraintForMessageTable!.constant = 40
		UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
			self.view.layoutIfNeeded()
		}, completion: nil)
	}
	
	// MARK: - Animations
	
	func sizeAnimation(fromValue: CGFloat, toValue: CGFloat, duration: CFTimeInterval, keyValue: String?) -> CABasicAnimation {
		let animation = CABasicAnimation(keyPath: "transform.scale")
		animation.fromValue = fromValue
		animation.toValue = toValue
		animation.duration = duration
		animation.isRemovedOnCompletion = false
		animation.fillMode = kCAFillModeBoth
		if let key = keyValue {
			animation.setValue(key, forKey: key)
			animation.delegate = self
		}
		return animation
	}
}

// MARK: - IConversationConnection

extension ConversationViewController: IConversationConnection {
	
	func didResumeConnecton() {
		self.isOnline = true
		var sizeForSendBtn: CABasicAnimation? = nil
		
		let sizeForTitle = sizeAnimation(fromValue: 1, toValue: 1.1, duration: 1, keyValue: nil)
		DispatchQueue.main.async {
			if self.newMessageTextField.text != "" {
				sizeForSendBtn = self.sizeAnimation(fromValue: 1, toValue: 1.15, duration: 0.5, keyValue: "sizeBigger")
				self.sendBtn.isEnabled = true
			}
			self.titleLabel?.changeTextColor(text: nil, color: #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1), duration: 1)
			self.titleLabel?.layer.add(sizeForTitle, forKey: nil)
			if let size = sizeForSendBtn {
				self.sendBtn.layer.add(size, forKey: nil)
			}
			if let frame = self.titleLabel?.frame {
				self.titleLabel?.layer.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
			}
		}
	}
	
	func didLostConnection() {
		self.isOnline = false
		var sizeForSendBtn: CABasicAnimation? = nil
		DispatchQueue.main.async {
			if self.newMessageTextField.text != "" {
				sizeForSendBtn = self.sizeAnimation(fromValue: 1, toValue: 1.15, duration: 0.5, keyValue: "sizeBigger")
				self.sendBtn.isEnabled = false
			}
			if let size = sizeForSendBtn {
				self.sendBtn.layer.add(size, forKey: nil)
			}
			let sizeForTitle = self.sizeAnimation(fromValue: 1.1, toValue: 1, duration: 1, keyValue: nil)
			self.titleLabel?.changeTextColor(text: nil, color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), duration: 1)
			self.titleLabel?.layer.add(sizeForTitle, forKey: nil)
		}
	}
}

// MARK: - Table View

extension ConversationViewController: UITableViewDelegate, UITableViewDataSource {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let frc = dataProvider?.frc, let sections = frc.sections else { return 0 }
		print("Number is \(sections[section].numberOfObjects)")
		return sections[section].numberOfObjects
	}
		
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		guard let message = dataProvider?.frc?.object(at: indexPath) else {
				print("Can't do something")
				return UITableViewCell()
		}
		
		let reuseIdentifier = message.type == "Income" ? "cellForIncomeMessage" : "cellForOutcomeMessage"
		let amISender = message.type == "Income" ? false : true
		let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! MessageTableViewCell
		cell.configure(with: message, amISender: amISender)
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
	}
	
	// MARK: - Delete Messages
	
	func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
		let delete = UITableViewRowAction(style: .default, title: "Удалить") { (action, indexPath) in
			let cell = tableView.cellForRow(at: indexPath) as? MessageTableViewCell
			guard let messageID = cell?.messageID else { return }
			self.model?.deleteMessage(withID: messageID)
			
			if indexPath.row + 1 == tableView.numberOfRows(inSection: 0) {
				if indexPath.row > 0 {
					let indexPathForNewLastMessage = IndexPath(row: indexPath.row - 1, section: 0)
					let newLastCell = tableView.cellForRow(at: indexPathForNewLastMessage) as? MessageTableViewCell
					guard let messageIDForNewLastMessage = newLastCell?.messageID else { return }
					self.model?.changeLastMessageForConversation(withID: self.conversationID, messageID: messageIDForNewLastMessage)
				}
			}
		}
		delete.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
		return [delete]
	}
}

// MARK: - Gesture Recognizer

extension ConversationViewController: UIGestureRecognizerDelegate {
	func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return true
	}
	
	@objc func hideKBOnTap() {
		self.view.endEditing(true)
	}
}

extension ConversationViewController: CAAnimationDelegate {
	func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
		guard let key = anim.value(forKey: "sizeBigger") as? String, key == "sizeBigger" else { return }
		let sizeAnimation = self.sizeAnimation(fromValue: 1.15, toValue: 1, duration: 0.5, keyValue: nil)
		sendBtn.layer.add(sizeAnimation, forKey: nil)
	}
}
