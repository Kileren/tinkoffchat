//
//  LogoEmitter.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 26.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation
import UIKit

protocol ILogoEmitter: class {
	func startEmitting(atPoint point: CGPoint)
	func stopEmitting()
	func makeEmitterCell() -> CAEmitterCell
}

class LogoEmitter: NSObject, ILogoEmitter {
	
	let view: UIView
	var longPressGesture = UILongPressGestureRecognizer()
	let emitter = CAEmitterLayer()
	
	init(view: UIView) {
		self.view = view
		super.init()
		longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.longPress))
		longPressGesture.minimumPressDuration = 0.1
		view.addGestureRecognizer(longPressGesture)
	}
	
	@objc func longPress() {
		if longPressGesture.state == .began || longPressGesture.state == .changed {
			startEmitting(atPoint: longPressGesture.location(in: self.view))
		} else if longPressGesture.state == .cancelled || longPressGesture.state == .ended {
			stopEmitting()
		}
	}
	
	func startEmitting(atPoint point: CGPoint) {
		emitter.emitterPosition = point
		emitter.emitterCells = [makeEmitterCell()]
		view.layer.addSublayer(emitter)
	}
	
	func stopEmitting() {
		emitter.removeFromSuperlayer()
	}
	
	func makeEmitterCell() -> CAEmitterCell {
		let cell = CAEmitterCell()
		cell.contents = UIImage(named: "Logo")?.cgImage
		cell.contentsScale = 10
		cell.birthRate = 5
		cell.lifetime = 1.5
		cell.lifetimeRange = 0.5
		cell.velocity = 0
		cell.velocityRange = 30
		cell.emissionLongitude = CGFloat.pi / 2
		cell.emissionRange = CGFloat.pi / 8
		cell.scaleRange = 0.5
		cell.scaleSpeed = 0.1
		cell.spinRange = 4
		return cell
	}
}
