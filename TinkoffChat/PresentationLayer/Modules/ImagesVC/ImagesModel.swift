//
//  ImagesModel.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 17.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation
import UIKit

protocol IImagesModel: class {
	var delegate: IImagesModelDelegate? { get set }
	func loadImages(withRequest request: String?)
}

protocol IImagesModelDelegate: class {
	func refreshImages(with imageURLs: [String])
}

class ImagesModel: IImagesModel {
	
	var delegate: IImagesModelDelegate?
	let imagesService: ImagesService
	
	init(imagesService: ImagesService) {
		self.imagesService = imagesService
	}
	
	// MARK: - IImagesModel
	
	func loadImages(withRequest request: String?) {
		
		imagesService.loadImages(request: request) { (images: ImageApiModel?, error: String?) in
			guard error == nil else {
				print(error!)
				return
			}
			guard let images = images else {
				print("There are no images")
				return
			}
			
			self.delegate?.refreshImages(with: images.webformatURLs)
		}
	}
}
