//
//  ImageCollectionViewCell.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 17.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
	
	@IBOutlet weak var imageView: UIImageView!
	
	func configure(withImageURL imageURL: String, completionHandler: @escaping (UIImage?) -> Void) {
		imageView.image = UIImage(named: "PlaceholderForImages")
		DispatchQueue.global(qos: .utility).async {
			guard let imageURL = URL(string: imageURL) else {
				print("Can't make URL for image from String")
				return
			}
			do {
				let data = try Data.init(contentsOf: imageURL)
				guard let newImage = UIImage(data: data) else {
					print("Can't make UIImage from Data")
					return
				}
				DispatchQueue.main.async {
					self.imageView.image = newImage
				}
				completionHandler(newImage)
			} catch {
				print(error.localizedDescription)
			}
		}
	}
	
	func configureWithExistImage(image: UIImage) {
		self.imageView.image = image
	}
}
