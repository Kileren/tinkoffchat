//
//  ImagesViewController.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 16.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import UIKit

protocol IImageProfileDelegate: class {
	func refreshProfileImage(with image: UIImage)
}

class ImagesViewController: UIViewController {
	
	// MARK: - Declaration

	@IBOutlet weak var imagesCollectionView: UICollectionView!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	@IBOutlet weak var searchTextField: UITextField!
	@IBOutlet weak var searchBtn: UIButton!
	
	private let itemsPerRow: CGFloat = 3
	private let sectionInsets = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
	
	var bottomConstraintForCollectionView: NSLayoutConstraint?
	
	var profileDelegate: IImageProfileDelegate?
	var model: IImagesModel?
	var logoEmitter: ILogoEmitter?
	
	var imageURLs: [String] = []
	var images: [Int: UIImage] = [:]
	
	// MARK: - Loading
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
		activityIndicator.hidesWhenStopped = true
		activityIndicator.startAnimating()
		
		configureImagesCollectionView()
		addSwipeDownForKeyboardHiding()
		addAdditionalConstraints()
		registerForNotifications()
		
		logoEmitter = LogoEmitter(view: self.view)
		
		guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
		model = ImagesModel(imagesService: appDelegate.rootAssembly.imagesService)
		model?.delegate = self
		
		DispatchQueue.global(qos: .utility).async {
			self.model?.loadImages(withRequest: nil)
		}
    }
	
	func configureImagesCollectionView() {
		imagesCollectionView.delegate = self
		imagesCollectionView.dataSource = self
	}
	
	func addSwipeDownForKeyboardHiding() {
		let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.hideKBOnTap))
		swipeDown.direction = .down
		swipeDown.delegate = self
		self.view.addGestureRecognizer(swipeDown)
	}
	
	func addAdditionalConstraints() {
		bottomConstraintForCollectionView = NSLayoutConstraint(item: imagesCollectionView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
		view.addConstraint(bottomConstraintForCollectionView!)
	}
	
	deinit {
		removeNotifications()
	}
	
	// MARK: - Buttons Actions
	
	@IBAction func searchBtnAction(_ sender: UIButton) {
		guard let textInTextField = searchTextField.text else { return }
		let text = checkForSpaces(in: textInTextField)
		print(text)
		self.view.endEditing(true)
		self.imageURLs.removeAll()
		self.images.removeAll()
		self.imagesCollectionView.reloadData()
		activityIndicator.startAnimating()
		
		DispatchQueue.global(qos: .utility).async {
			self.model?.loadImages(withRequest: text)
		}
	}
	
	@IBAction func closeBtnAction(_ sender: UIButton) {
		self.imageURLs.removeAll()
		self.images.removeAll()
		self.model = nil
		self.dismiss(animated: true, completion: nil)
	}
	
	// MARK: - Additional Functions
	
	func checkForSpaces(in text: String) -> String {
		var changedText = ""
		if !text.contains(" ") { return text }
		for symbol in text {
			if symbol == " " {
				changedText += "+"
				continue
			}
			changedText += String(symbol)
		}
		return changedText
	}
	
	// MARK: - Notifications
	
	func registerForNotifications() {
		NotificationCenter.default.addObserver(self, selector: #selector(kbWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(kbWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
	}
	
	func removeNotifications() {
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
	}
	
	@objc func kbWillShow(_ notification: Notification) {
		let userInfo = notification.userInfo
		let kbFrameSize = (userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
		bottomConstraintForCollectionView?.constant = -kbFrameSize.height
		UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
			self.view.layoutIfNeeded()
		}, completion: nil)
	}
	
	@objc func kbWillHide() {
		bottomConstraintForCollectionView?.constant = 0
	}
}

// MARK: - IImagesModelDelegate

extension ImagesViewController: IImagesModelDelegate {
	
	func refreshImages(with imageURLs: [String]) {
		self.imageURLs = imageURLs
		DispatchQueue.main.async {
			self.activityIndicator.stopAnimating()
			self.imagesCollectionView.reloadData()
		}
	}
}

// MARK: - CollectionViewDelegate & DataSource

extension ImagesViewController: UICollectionViewDelegate, UICollectionViewDataSource {
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return imageURLs.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCollectionViewCell", for: indexPath) as? ImageCollectionViewCell else {
			return collectionView.dequeueReusableCell(withReuseIdentifier: "imageCollectionViewCell", for: indexPath)
		}
		
		if let existImage = images[indexPath.row] {
			cell.configureWithExistImage(image: existImage)
			return cell
		}
		if imageURLs.count >= indexPath.row {
			let imageURL = self.imageURLs[indexPath.row]
			cell.configure(withImageURL: imageURL) { (image: UIImage?) in
				if let image = image {
					self.images[indexPath.row] = image
				}
			}
			return cell
		}
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let cell = collectionView.cellForItem(at: indexPath) as? ImageCollectionViewCell
		if let image = cell?.imageView.image {
			self.profileDelegate?.refreshProfileImage(with: image)
		}
		self.dismiss(animated: true, completion: nil)
	}
}

// MARK: - CollectionViewDelegateFlowLayout

extension ImagesViewController: UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView,
						layout collectionViewLayout: UICollectionViewLayout,
						sizeForItemAt indexPath: IndexPath) -> CGSize {
		
		let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
		let widthForItems = view.frame.width - paddingSpace
		let widthPerItem = widthForItems / itemsPerRow
		
		return CGSize(width: widthPerItem, height: widthPerItem)
	}
	
	func collectionView(_ collectionView: UICollectionView,
						layout collectionViewLayout: UICollectionViewLayout,
						insetForSectionAt section: Int) -> UIEdgeInsets {
		
		return sectionInsets
	}
	
	func collectionView(_ collectionView: UICollectionView,
						layout collectionViewLayout: UICollectionViewLayout,
						minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		
		return sectionInsets.left
	}
}

// MARK: - UIGestureRecognizerDelegate

extension ImagesViewController: UIGestureRecognizerDelegate {
	func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return true
	}
	
	@objc func hideKBOnTap() {
		self.view.endEditing(true)
	}
}
