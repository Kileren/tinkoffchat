//
//  ViewController.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 20.09.17.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, CAAnimationDelegate, IProfileModelDelegate {
	
	func changeProfileInfo(with profileInfo: ProfileInfo) {
		self.avatarImageView.image = profileInfo.image
		self.nameTextField.text = profileInfo.name
		self.aboutMyselfTextView.text = profileInfo.aboutInfo
	}
	
	// MARK: - Declaration
	
	@IBOutlet weak var gcdBtn: UIButton!
	@IBOutlet weak var operationBtn: UIButton!
	@IBOutlet weak var saveBtn: UIButton!
	@IBOutlet weak var cameraBtn: UIButton!
	
	@IBOutlet weak var nameTextField: UITextField!
	@IBOutlet weak var aboutMyselfTextView: UITextView!
	@IBOutlet weak var avatarImageView: UIImageView!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	@IBOutlet weak var scrollView: UIScrollView!
	
	var isNameChanged = false
	var isAboutMeChanged = false
	var isAvatarChanged = false
	
	var isSaveInProccess = false
	var neededToChangeBtnState = false
	
	var model: ProfileModel?
	var logoEmitter: ILogoEmitter?
	var logoEmitterForNavigationController: ILogoEmitter?
	
	// MARK: - Loading
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		makeEmitters()
		addTapForKeyboardHiding()
		registerForNotification()
		
		guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
		self.model = getModel(appDelegate: appDelegate)
		self.model?.delegate = self
		model?.retrieveProfileInfo()
		
		setButtons(toState: false)
	}
	
	func makeEmitters() {
		logoEmitter = LogoEmitter(view: self.view)
		if let navigationView = self.navigationController?.view {
			logoEmitterForNavigationController = LogoEmitter(view: navigationView)
		}
	}
	
	func addTapForKeyboardHiding() {
		let tap = UITapGestureRecognizer(target: self, action: #selector(self.hideKBOnTap))
		tap.delegate = self
		self.view.addGestureRecognizer(tap)
	}
	
	func getModel(appDelegate: AppDelegate) -> ProfileModel {
		return ProfileModel(profileService: appDelegate.rootAssembly.profileService)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		// Кнопка камеры
		cameraBtn.layer.masksToBounds = true
		cameraBtn.layer.cornerRadius = cameraBtn.layer.frame.width / 2

		// Фото профиля
		avatarImageView.layer.cornerRadius = cameraBtn.frame.width / 2
		avatarImageView.layer.masksToBounds = true

		// Кнопка GCD
		gcdBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		gcdBtn.layer.borderWidth = 2
		gcdBtn.layer.cornerRadius = gcdBtn.frame.height / 4

		// Кнопка Operation
		operationBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		operationBtn.layer.borderWidth = 2
		operationBtn.layer.cornerRadius = operationBtn.frame.height / 4
		
		// Кнопка Save
		saveBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		saveBtn.layer.borderWidth = 2
		saveBtn.layer.cornerRadius = saveBtn.frame.height / 4
	}
	
	// MARK: - Buttons Actions
	
	@IBAction func cancelAction(_ sender: UIBarButtonItem) {
		dismiss(animated: true, completion: nil)
	}

	@IBAction func cameraAction(_ sender: UIButton) {
		print("Выберите изображение профиля")
		
		let alertController = UIAlertController(title: "Выберите источник фотографии:", message: nil, preferredStyle: .actionSheet)
		let camera = UIAlertAction(title: "Камера", style: .default) { (action) in
			self.presentImagePickerAction(withSource: .camera)
		}
		let photoLibrary = UIAlertAction(title: "Фото", style: .default) { (action) in
			self.presentImagePickerAction(withSource: .photoLibrary)
		}
		let loadFromNetwork = UIAlertAction(title: "Загрузить", style: .default) { (action) in
			guard let imagesVC = UIStoryboard(name: "ImagesFromTheInternet", bundle: nil).instantiateInitialViewController() as? ImagesViewController else {
				return
			}
			imagesVC.profileDelegate = self
			self.present(imagesVC, animated: true, completion: nil)
		}
		let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
		alertController.addAction(camera)
		alertController.addAction(photoLibrary)
		alertController.addAction(loadFromNetwork)
		alertController.addAction(cancel)
		self.present(alertController, animated: true, completion: nil)
	}

	@IBAction func gcdBtnAction(_ sender: UIButton) {
		
		if nameTextField.text == "" { nameTextField.text = "-"}
		
		let gcdManager = GCDDataManager(name: (nameTextField.text, isNameChanged), aboutMyself: (aboutMyselfTextView.text, isAboutMeChanged), avatar: (avatarImageView.image!, isAvatarChanged))
		
		setButtons(toState: false)
		isSaveInProccess = true
		activityIndicator.startAnimating()
		// Запись данных в файл + хэндлер
		gcdManager.writeToFile { (isSuccess) in
			DispatchQueue.main.async {
				self.activityIndicator.stopAnimating()
				if isSuccess {
					self.isNameChanged = false
					self.isAboutMeChanged = false
					self.isAvatarChanged = false
					self.showSuccessAlert()
				} else {
					self.showErrorAlert(name: self.nameTextField.text, aboutMyself: self.aboutMyselfTextView.text, avatar: self.avatarImageView.image!, methodName: "gcd")
				}
			}
		}
	}

	@IBAction func operationBtnAction(_ sender: UIButton) {
		
		if nameTextField.text == "" { nameTextField.text = "-"}
		
		let operationDataManager = OperationDataManager(name: (nameTextField.text, isNameChanged), aboutMyself: (aboutMyselfTextView.text, isAboutMeChanged), avatar: (avatarImageView.image!, isAvatarChanged))
		
		setButtons(toState: false)
		isSaveInProccess = true
		activityIndicator.startAnimating()
		// Запись данных в файл + хэндлер
		operationDataManager.writeToFile { (isSuccess) in
			let mainQueue = OperationQueue.main
			mainQueue.addOperation {
				self.activityIndicator.stopAnimating()
				if isSuccess {
					self.showSuccessAlert()
					self.isNameChanged = false
					self.isAboutMeChanged = false
					self.isAvatarChanged = false
				} else {
					self.showErrorAlert(name: self.nameTextField.text, aboutMyself: self.aboutMyselfTextView.text, avatar: self.avatarImageView.image!, methodName: "operation")
				}
			}
		}
	}
	
	@IBAction func saveBtnAction(_ sender: UIButton) {
		
		if let profileModel = model {
			if let image = avatarImageView.image, let name = nameTextField.text, let aboutInfo = aboutMyselfTextView.text {
				let info = ProfileInfo(image: image, name: name, aboutInfo: aboutInfo)
				profileModel.saveProfileInfo(info: info)
			} else {
				print("Error with unwrapping")
			}
		} else {
			print("Error with model")
		}
		
		self.view.endEditing(true)
		createAnimationForSaveBtnOpacity(fromValue: 1, toValue: 0.3, saveBtnState: false, shouldHasDelegate: true)
	}
	
	// MARK: - Functions
	
	func readFromFile() {
		// Чтение с использованием GCD
		let gcdManager = GCDDataManager()
		gcdManager.readFromFile { (name, aboutMyself, avatar) in
			DispatchQueue.main.async {
				self.nameTextField.text = name
				self.aboutMyselfTextView.text = aboutMyself
				self.avatarImageView.image = avatar
			}
		}
		
		// Чтение с использованием Operations
//		let operationManager = OperationDataManager()
//		operationManager.readFromFile { (name, aboutMyself, avatar) in
//			let mainQueue = OperationQueue.main
//			mainQueue.addOperation {
//				self.nameTextField.text = name
//				self.aboutMyselfTextView.text = aboutMyself
//				self.avatarImageView.image = avatar
//			}
//		}
	}
	
	func showSuccessAlert() {
		let alertController = UIAlertController(title: "Данные сохранены", message: nil, preferredStyle: .alert)
		let okBtn = UIAlertAction(title: "ОК", style: .cancel, handler: nil)
		alertController.addAction(okBtn)
		present(alertController, animated: true) {
			self.isSaveInProccess = false
			if self.neededToChangeBtnState {
				self.setButtons(toState: true)
				self.neededToChangeBtnState = false
			}
		}
	}
	
	func showErrorAlert(name: String?, aboutMyself: String?, avatar: UIImage, methodName: String) {
		let alertController = UIAlertController(title: "Ошибка", message: "Не удалось сохранить данные", preferredStyle: .alert)
		let okBtn = UIAlertAction(title: "ОК", style: .cancel) { (action) in
			self.setButtons(toState: true)
			self.isSaveInProccess = false
			self.neededToChangeBtnState = false
		}
		// Кнопка "Повторить"
		let retryBtn = UIAlertAction(title: "Повторить", style: .default) { (action) in
			
			self.activityIndicator.startAnimating()
			
			if methodName == "gcd" {
				// Если был использован GCD
				let gcdManager = GCDDataManager(name: (self.nameTextField.text, self.isNameChanged), aboutMyself: (self.aboutMyselfTextView.text, self.isAboutMeChanged), avatar: (self.avatarImageView.image!, self.isAvatarChanged))
				gcdManager.writeToFile(completion: { (isSuccess) in
					DispatchQueue.main.async {
						self.eventsAfterRetrySaveAction(isSuccessWrite: isSuccess, methodName: methodName)
					}
				})
			} else {
				// Если был использован operation
				let operationDataManager = OperationDataManager(name: (self.nameTextField.text, self.isNameChanged), aboutMyself: (self.aboutMyselfTextView.text, self.isAboutMeChanged), avatar: (self.avatarImageView.image!, self.isAvatarChanged))
				operationDataManager.writeToFile(completion: { (isSuccess) in
					let mainQueue = OperationQueue.main
					mainQueue.addOperation {
						self.eventsAfterRetrySaveAction(isSuccessWrite: isSuccess, methodName: methodName)
					}
				})
			}
		}
		alertController.addAction(okBtn)
		alertController.addAction(retryBtn)
		present(alertController, animated: true, completion: nil)
	}
	
	// События после нажатия на кнопку "Повторить" и выполнения попытки записи данных при предыдущей неудачной записи данных
	func eventsAfterRetrySaveAction(isSuccessWrite: Bool, methodName: String) {
		self.activityIndicator.stopAnimating()
		if isSuccessWrite {
			self.showSuccessAlert()
			self.isNameChanged = false
			self.isAboutMeChanged = false
			self.isAvatarChanged = false
		} else {
			self.showErrorAlert(name: self.nameTextField.text, aboutMyself: self.aboutMyselfTextView.text, avatar: self.avatarImageView.image!, methodName: methodName)
		}
	}
	
	// Выставление кнопок gcd и operation в активное/неактивное состояние
	func setButtons(toState state: Bool) {
		gcdBtn.isEnabled = state
		operationBtn.isEnabled = state
		if state == false {
			gcdBtn.alpha = 0.5
			operationBtn.alpha = 0.5
		} else {
			gcdBtn.alpha = 1
			operationBtn.alpha = 1
		}
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	// MARK: - Notifications (Keyboard, value changes)
	
	func registerForNotification() {
		NotificationCenter.default.addObserver(self, selector: #selector(self.kbWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.kbWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.textViewChanged), name: NSNotification.Name.UITextViewTextDidChange, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.textFieldChanged), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
	}
	
	func removeNotifications() {
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UITextViewTextDidChange, object: nil)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
	}
	
	deinit {
		removeNotifications()
	}
	
	@objc func kbWillShow(_ notification: Notification) {
		let userInfo = notification.userInfo
		let kbFrameSize = (userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
		scrollView.contentOffset = CGPoint(x: 0, y: kbFrameSize.height)
	}
	
	@objc func kbWillHide() {
		scrollView.contentOffset = CGPoint.zero
	}
	
	@objc func textViewChanged() {
		if !isSaveInProccess { setButtons(toState: true) } else { neededToChangeBtnState = true }
		isAboutMeChanged = true
	}
	
	@objc func textFieldChanged() {
		if !isSaveInProccess { setButtons(toState: true) } else { neededToChangeBtnState = true }
		isNameChanged = true
	}
	
	// MARK: - Animations
	
	func createAnimationForSaveBtnOpacity(fromValue: Double, toValue: Double, saveBtnState: Bool, shouldHasDelegate: Bool) {
		let animation = CABasicAnimation(keyPath: "opacity")
		animation.fromValue = fromValue
		animation.toValue = toValue
		animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionDefault)
		animation.fillMode = kCAFillModeBoth
		animation.isRemovedOnCompletion = false
		if shouldHasDelegate { animation.delegate = self }
		saveBtn.layer.add(animation, forKey: nil)
		saveBtn.isEnabled = saveBtnState
	}

	func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
		createAnimationForSaveBtnOpacity(fromValue: 0.3, toValue: 1, saveBtnState: true, shouldHasDelegate: false)
	}
}

// MARK: - ImagePicker

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

	func presentImagePickerAction(withSource source: UIImagePickerControllerSourceType) {
		guard UIImagePickerController.isSourceTypeAvailable(source) else { return }
		let imagePicker = UIImagePickerController()
		imagePicker.delegate = self
		imagePicker.allowsEditing = true
		imagePicker.sourceType = source
		self.present(imagePicker, animated: true, completion: nil)
	}

	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		avatarImageView.image = info[UIImagePickerControllerEditedImage] as? UIImage
		avatarImageView.contentMode = .scaleAspectFill
		avatarImageView.clipsToBounds = true
		if !isSaveInProccess { setButtons(toState: true) } else { neededToChangeBtnState = true }
		isAvatarChanged = true
		dismiss(animated: true, completion: nil)
	}
}

// MARK: - Gesture Recognizer

extension ProfileViewController: UIGestureRecognizerDelegate {
	func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return true
	}
	
	@objc func hideKBOnTap() {
		self.view.endEditing(true)
	}
}

// MARK: - IImageProfileDelegate

extension ProfileViewController: IImageProfileDelegate {
	func refreshProfileImage(with image: UIImage) {
		avatarImageView.image = image
	}
}
