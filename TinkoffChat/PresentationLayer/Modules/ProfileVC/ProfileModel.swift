//
//  ProfileModel.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 05.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation

protocol IProfileModel: class {
	var delegate: IProfileModelDelegate? { get set }
	func saveProfileInfo(info: ProfileInfo)
	func retrieveProfileInfo()
}

protocol IProfileModelDelegate: class {
	func changeProfileInfo(with profileInfo: ProfileInfo)
}

class ProfileModel: IProfileModel, IProfileServiceDelegate {
	
	var delegate: IProfileModelDelegate?
	let profileService: ProfileService
	
	init(profileService: ProfileService) {
		self.profileService = profileService
		self.profileService.delegate = self
	}
	
	func saveProfileInfo(info: ProfileInfo) {
		profileService.saveProfileInfo(info: info)
	}
	
	func retrieveProfileInfo() {
		profileService.retrieveProfileInfo()
	}
	
	func gotProfileInfo(info: ProfileInfo) {
		delegate?.changeProfileInfo(with: info)
	}
}
