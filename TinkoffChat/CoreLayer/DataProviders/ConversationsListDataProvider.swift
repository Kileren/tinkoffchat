//
//  ConversationsListDataProvider.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 11.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class ConversationsListDataProvider: NSObject {
	
	let frc: NSFetchedResultsController<Conversation>?
	let tableView: UITableView
	
	init(tableView: UITableView, coreDataStack: CoreDataStack) {
		self.tableView = tableView
		
		guard let context = coreDataStack.mainContext, let model = context.persistentStoreCoordinator?.managedObjectModel else {
			self.frc = nil
			super.init()
			return
		}
		
		let fetchRequests = FetchRequests(model: model)
		guard let fetchRequest = fetchRequests.fetchRequestConversations() else {
			self.frc = nil
			super.init()
			return
		}
		
		let dateDescriptor = NSSortDescriptor(key: #keyPath(Conversation.lastMessage.date), ascending: false)
		let onlineDescriptor = NSSortDescriptor(key: #keyPath(Conversation.isOnline), ascending: false)
		fetchRequest.sortDescriptors = [onlineDescriptor, dateDescriptor]
		
        frc = NSFetchedResultsController<Conversation>(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: #keyPath(Conversation.isOnline), cacheName: nil)
		super.init()
		frc?.delegate = self
		
		do {
			try frc?.performFetch()
		} catch {
			print("Error fetching: \(error)")
		}
	}
}

extension ConversationsListDataProvider: NSFetchedResultsControllerDelegate {
	
	func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		tableView.beginUpdates()
	}
	
	func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		tableView.endUpdates()
	}
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
		
		switch type {
		case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
		case .insert:
			if let newIndexPath = newIndexPath {
				tableView.insertRows(at: [newIndexPath], with: .automatic)
			}
		case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
            
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
		case .update:
            if let indexPath = indexPath {
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
		}
	}
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .automatic)
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .automatic)
        case .move, .update:
            break
        }
	}
}
