//
//  ConversationDataProvider.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 11.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class ConversationDataProvider: NSObject {
	
	var frc: NSFetchedResultsController<Message>?
	let tableView: UITableView
	let conversationID: String
	
	init(tableView: UITableView, coreDataStack: CoreDataStack, conversationID: String) {
		self.tableView = tableView
		self.conversationID = conversationID
		
		guard let context = coreDataStack.mainContext, let model = context.persistentStoreCoordinator?.managedObjectModel else {
			self.frc = nil
			super.init()
			return
		}
		
		let fetchRequests = FetchRequests(model: model)
		guard let fetchRequest = fetchRequests.fetchRequestMessagesInConversationForID(id: self.conversationID, in: model) else {
			self.frc = nil
			super.init()
			return
		}
		
		let sortDescriptor = NSSortDescriptor(key: #keyPath(Message.date), ascending: true)
		fetchRequest.sortDescriptors = [sortDescriptor]
		
		self.frc = NSFetchedResultsController<Message>(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
		super.init()
		frc?.delegate = self
		
		do {
			try frc?.performFetch()
		} catch {
			print("Error fetching: \(error)")
		}
	}
}

extension ConversationDataProvider: NSFetchedResultsControllerDelegate {
	
	func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		tableView.beginUpdates()
	}
	
	func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		tableView.endUpdates()
	}
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
		switch type {
		case .delete:
			if let indexPath = indexPath {
				tableView.deleteRows(at: [indexPath], with: .automatic)
			}
		case .insert:
			if let newIndexPath = newIndexPath {
				tableView.insertRows(at: [newIndexPath], with: .automatic)
				DispatchQueue.main.async {
					self.tableView.scrollToLastCell(animated: false, afterNewMessage: true, section: 0)
				}
			}
		case .move:
			if let indexPath = indexPath {
				tableView.deleteRows(at: [indexPath], with: .automatic)
			}
			
			if let newIndexPath = newIndexPath {
				tableView.insertRows(at: [newIndexPath], with: .automatic)
			}
		case .update:
			if let indexPath = indexPath {
				tableView.reloadRows(at: [indexPath], with: .automatic)
			}
		}
	}
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
		switch type {
		case .delete:
			tableView.deleteSections(IndexSet(integer: sectionIndex), with: .automatic)
		case .insert:
			tableView.insertSections(IndexSet(integer: sectionIndex), with: .automatic)
		case .move, .update:
			break
		}
	}
}
