//
//  OperationDataManager.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 16.10.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation
import UIKit

class OperationDataManager: Operation, DataToSave {
	
	// MARK: - Declaration
	
	var nameToSave: (String?, Bool)
	var aboutMyselfToSave: (String?, Bool)
	var avatarToSave: (UIImage, Bool)
	
	// MARK: - Initializers
	
	init(name: (String?, Bool), aboutMyself: (String?, Bool), avatar: (UIImage, Bool)) {
		self.nameToSave = name
		self.aboutMyselfToSave = aboutMyself
		self.avatarToSave = avatar
	}
	
	convenience override init() {
		self.init(name: ("", true), aboutMyself: ("", true), avatar: (UIImage.init(), true))
	}
	
	// MARK: - Declaration
	
	func writeToFile(completion: @escaping (_ success: Bool) -> ()) {
		
		let queue = OperationQueue()
		queue.addOperation {
			let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first! + "/Data.plist"
			
			guard let dictionary = NSMutableDictionary(contentsOfFile: path) else {
				let dictionary = [:] as NSMutableDictionary
				if self.nameToSave.1 { dictionary["name"] = self.nameToSave.0 }
				if self.aboutMyselfToSave.1 { dictionary["aboutMyself"] = self.aboutMyselfToSave.0 }
				if self.avatarToSave.1 { dictionary["avatar"] = UIImagePNGRepresentation(self.avatarToSave.0) }
				
				let isSuccess = dictionary.write(toFile: path, atomically: true)
				completion(isSuccess)
				return
			}
			
			if self.nameToSave.1 { dictionary["name"] = self.nameToSave.0 }
			if self.aboutMyselfToSave.1 { dictionary["aboutMyself"] = self.aboutMyselfToSave.0 }
			if self.avatarToSave.1 { dictionary["avatar"] = UIImagePNGRepresentation(self.avatarToSave.0) }
			
			let isSuccess = dictionary.write(toFile: path, atomically: true)
			completion(isSuccess)
		}
	}
	
	func readFromFile(completion: @escaping (_ name: String, _ aboutMyself: String, _ avatar: UIImage) -> ()) {
		
		let queue = OperationQueue()
		queue.addOperation {
			let pathToFile = (NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first! + "/Data.plist")
			guard let dictionary = NSDictionary(contentsOfFile: pathToFile) else { print("NoData"); return }
			
			var name: String!
			var aboutMyself: String!
			var avatar: UIImage!
			
			for data in dictionary {
				let key = data.key as! String
				switch key {
				case "name":
					name = data.value as? String
				case "aboutMyself":
					aboutMyself = data.value as? String
				case "avatar":
					avatar = UIImage(data: data.value as! Data)
				default:
					continue
				}
			}
			completion(name, aboutMyself!, avatar)
		}
	}
}
