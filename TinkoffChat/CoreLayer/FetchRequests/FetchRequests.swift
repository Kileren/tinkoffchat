//
//  FetchRequests.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 11.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation
import CoreData

protocol IFetchRequests: class {
	func fetchRequestUsersOnline() -> NSFetchRequest<User>?
	func fetchRequestUser(withUserID userID: String) -> NSFetchRequest<User>?
	func fetchRequestConversations() -> NSFetchRequest<Conversation>?
	func fetchRequestMessagesInConversationForID(id: String, in model: NSManagedObjectModel) -> NSFetchRequest<Message>?
	func fetchRequestMessageWithID(id: String) -> NSFetchRequest<Message>?
}

class FetchRequests {
	
    let model: NSManagedObjectModel
    
    init(model: NSManagedObjectModel) {
        self.model = model
    }
}

extension FetchRequests: IFetchRequests {
	
	func fetchRequestUsersOnline() -> NSFetchRequest<User>? {
		return User.fetchRequestUsersOnline(model: model)
	}
    
	func fetchRequestUser(withUserID userID: String) -> NSFetchRequest<User>? {
		return User.fetchRequestUser(withUserID: userID, model: model)
    }
	
	func fetchRequestConversations() -> NSFetchRequest<Conversation>? {
		let fetchRequest = NSFetchRequest<Conversation>(entityName: "Conversation")
		return fetchRequest
	}
    
	func fetchRequestMessagesInConversationForID(id: String, in model: NSManagedObjectModel) -> NSFetchRequest<Message>? {
		let fetchRequest = Message.fetchRequestMessagesForConversationID(id: id, model: model)
        return fetchRequest
    }
	
	func fetchRequestMessageWithID(id: String) -> NSFetchRequest<Message>? {
		let fetchRequest = Message.fetchRequestMessageWithID(id: id, model: model)
		return fetchRequest
	}
}
