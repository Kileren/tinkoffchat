//
//  IRequest.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 16.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation

protocol IRequest {
	var urlRequest: URLRequest? { get }
}
