//
//  PixabayRequest.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 16.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation

class PixabayRequest: IRequest {
	
	// MARK: - Declaration
	
	private var baseURL: String = "https://pixabay.com/api/"
	private var apiKey: String = "4269235-63acbccedc71473625c9d133c"
	private var additionalRequest: String
	private var per_page = "150"
	private var urlString: String {
		let baseURL = self.baseURL + "?key=" + self.apiKey
		let request = "&q=" + additionalRequest + "&image_type=all"
		let numberOfImages = "&per_page=" + per_page
		return baseURL + request + numberOfImages
	}
	
	// MARK: - Initialization
	
	init(request: String) {
		self.additionalRequest = request
	}
	
	convenience init() {
		self.init(request: "image")
	}
	
	// MARK: - IRequest
	
	var urlRequest: URLRequest? {
		if let url = URL(string: urlString) {
			return URLRequest(url: url)
		}
		return nil
	}
}
