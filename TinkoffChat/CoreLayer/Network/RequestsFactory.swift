//
//  RequestsFactory.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 16.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation

struct RequestsFactory {
	
	struct PixabayRequests {
		
		static func imagesConfig() -> RequestConfig<ImageApiModel> {
			let request = PixabayRequest()
			return RequestConfig<ImageApiModel>(request: request, parser: PixabayParser())
		}
		
		static func imagesConfigForSpecificRequest(request: String) -> RequestConfig<ImageApiModel> {
			let request = PixabayRequest(request: request)
			return RequestConfig<ImageApiModel>(request: request, parser: PixabayParser())
		}
	}
}
