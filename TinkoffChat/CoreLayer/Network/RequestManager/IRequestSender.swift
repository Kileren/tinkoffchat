//
//  IRequestSender.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 16.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation

struct RequestConfig<Model> where Model: IModel {
	let request: IRequest
	let parser: Parser<Model>
}

enum Result<T> {
	case success(T)
	case error(String)
}

protocol IRequestSender {
	func send<Model>(config: RequestConfig<Model>, completionHandler: @escaping(Result<Model>) -> Void)
}
