//
//  RequestSender.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 16.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation

class RequestSender: IRequestSender {
	
	let session = URLSession.shared
	
	func send<Model>(config: RequestConfig<Model>, completionHandler: @escaping (Result<Model>) -> Void) {
		guard let urlRequest = config.request.urlRequest else {
			completionHandler(Result.error("Url string can't be parsed to URL"))
			return
		}
		
		let task = session.dataTask(with: urlRequest) { (data, response, error) in
			if let error = error {
				completionHandler(Result.error(error.localizedDescription))
				return
			}
			guard let data = data, let parsedModel = config.parser.parse(data: data) else {
				completionHandler(Result.error("Received data can't be parse"))
				return
			}
			completionHandler(Result.success(parsedModel))
		}
		
		task.resume()
	}
}
