//
//  PixabayParser.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 16.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ImageApiModel: IModel {
	typealias Model = PixabayParser
	let webformatURLs: [String]
}

class PixabayParser: Parser<ImageApiModel> {
	
	override func parse(data: Data) -> ImageApiModel? {
		let json = JSON(data: data)
		guard let images = json["hits"].array else {
			return nil
		}

		var urls: [String] = []
		for image in images {
			guard let webformatURL = image["webformatURL"].string else {
					continue
			}
			urls.append(webformatURL)
		}
		return ImageApiModel(webformatURLs: urls)
	}
}
