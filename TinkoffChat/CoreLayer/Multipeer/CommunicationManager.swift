//
//  CommunicationManager.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 19.10.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation
import UIKit

protocol ICommunicatorManager: class {
	weak var delegate: ICommunicationManagerDelegate? { get set }
	weak var conversationDelegate: ICommunicationDialogManagerDelegate? { get set }
}

protocol ICommunicatorDialogManager: class {
	weak var conversationDelegate: ICommunicationDialogManagerDelegate? { get set }
	func sendMessage(message: MessageStruct, to user: String, completionHandler: ((_ success: Bool, _ error: Error?) -> ())?)
}

protocol ICommunicationManagerDelegate: class {
	func didFoundUser(withID userID: String, name: String)
	func didReceiveMessage(message: MessageStruct, from userID: String)
	func didLostUser(userID: String)
	func failedToStartBrowsingForUsers(error: Error)
	func failedToStartAdvertising(error: Error)
}

protocol ICommunicationDialogManagerDelegate: class {
	func didLostUser(userID: String)
	func didFoundUser(withID userID: String, name: String)
}

class CommunicationManager: NSObject, CommunicatorDelegate, ICommunicatorManager, ICommunicatorDialogManager {
	
	var delegate: ICommunicationManagerDelegate?
	var conversationDelegate: ICommunicationDialogManagerDelegate?
	
	var multipeerCommunicator: MultipeerCommunicator
	
	override init() {
		self.multipeerCommunicator = MultipeerCommunicator()
		super.init()
		multipeerCommunicator.delegate = self
	}
	
	// MARK: - ICommunicatorDialogManager
	
	func sendMessage(message: MessageStruct, to user: String, completionHandler: ((Bool, Error?) -> ())?) {
		multipeerCommunicator.sendMessage(message: message, to: user, completionHandler: completionHandler)
	}
	
	// MARK: - CommunicatorDelegate
	
	func didFoundUser(withID userID: String, name: String) {
		delegate?.didFoundUser(withID: userID, name: name)
		conversationDelegate?.didFoundUser(withID: userID, name: name)
	}
	
	func didLostUser(userID: String) {
		
		DispatchQueue.main.async {
			if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
				let storageManager = appDelegate.rootAssembly.storageManager
				storageManager.changeUserToOffline(withUserID: userID)
			}
		}
		
		delegate?.didLostUser(userID: userID)
		conversationDelegate?.didLostUser(userID: userID)
	}
	
	func failedToStartBrowsingForUsers(error: Error) {
		delegate?.failedToStartBrowsingForUsers(error: error)
	}
	
	func failedToStartAdvertising(error: Error) {
		delegate?.failedToStartAdvertising(error: error)
	}
	
	func didReceiveMessage(message: MessageStruct, fromUser: String, toUser: String) {
		DispatchQueue.main.async {
			guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
			appDelegate.rootAssembly.storageManager.newMessage(withText: message.text, messageID: message.messageID, type: "Income", from: fromUser, to: toUser)
		}
	}
}
