//
//  MultipeerCommunicator.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 19.10.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation
import UIKit
import MultipeerConnectivity

protocol Communicator {
	weak var delegate: CommunicatorDelegate? { get set }
	func sendMessage(message: MessageStruct, to user: String, completionHandler: ((_ success: Bool, _ error: Error?) -> ())?)
}

protocol CommunicatorDelegate: class {
	func didFoundUser(withID userID: String, name: String)
	func didLostUser(userID: String)
	
	func failedToStartBrowsingForUsers(error: Error)
	func failedToStartAdvertising(error: Error)
	
	func didReceiveMessage(message: MessageStruct, fromUser: String, toUser: String)
}

class MultipeerCommunicator: NSObject, Communicator {
	
	// MARK: - Declaration
	
	weak var delegate: CommunicatorDelegate?
	
	let myPeerID = MCPeerID(displayName: (UIDevice.current.identifierForVendor?.uuidString)!)
	let discoveryInfo = ["userName": "Sergey"]
	let serviceType = "tinkoff-chat"
	
	let serviceAdvertiser: MCNearbyServiceAdvertiser
	let serviceBrowser: MCNearbyServiceBrowser
	
	var users = [String: String]() // [userID: name]
	var sessions = [String: MCSession]() // [userID: session]
	
	let encoder = Encoder()
	
	// MARK: - Loading
	
	override init() {
		
		print((UIDevice.current.identifierForVendor?.uuidString)!)
		
		serviceAdvertiser = MCNearbyServiceAdvertiser(peer: myPeerID, discoveryInfo: discoveryInfo, serviceType: serviceType)
		serviceBrowser = MCNearbyServiceBrowser(peer: myPeerID, serviceType: serviceType)
		
		super.init()
		
		serviceAdvertiser.delegate = self
		serviceBrowser.delegate = self
		
		serviceAdvertiser.startAdvertisingPeer()
		serviceBrowser.startBrowsingForPeers()
	}
	
	deinit {
		serviceAdvertiser.stopAdvertisingPeer()
		serviceBrowser.stopBrowsingForPeers()
	}
	
	// MARK: - Communicator
	
	func sendMessage(message: MessageStruct, to user: String, completionHandler: ((Bool, Error?) -> ())?) {
		guard let currentSession = sessions[user] else { return }
		
		do {
			let dictionaryMessage = makeDictionaryFromMessage(message: message)
			let messageData = try JSONSerialization.data(withJSONObject: dictionaryMessage, options: .prettyPrinted)
			try currentSession.send(messageData, toPeers: currentSession.connectedPeers, with: .reliable)
			
			guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,
				let coreDataStack = appDelegate.rootAssembly.storageManager.coreDataStack,
				let context = coreDataStack.saveContext,
				let appUser = AppUser.findOrInsertAppUser(in: context),
				let userID = appUser.currentUser?.userID else { return }
			
			appDelegate.rootAssembly.storageManager.newMessage(withText: message.text, messageID: message.messageID, type: message.type, from: userID, to: user)
			
		} catch {
			if let handler = completionHandler {
				handler(false, error)
			}
		}
		
		if let handler = completionHandler {
			handler(true, nil)
		}
	}
	
	// MARK: - Functions
	
	func makeDictionaryFromMessage(message: MessageStruct) -> [String: String] {
		var dictionary = [String: String]()
		dictionary["eventType"] = message.eventType
		dictionary["messageID"] = message.messageID
		dictionary["text"] = message.text
		return dictionary
	}

	func findSession(withPeerID peerID: MCPeerID) -> MCSession? {
		for session in sessions {
			if session.key == peerID.displayName {
				return session.value
			}
		}
		return nil
	}
	
	func userConnected(withPeerID peerID: MCPeerID) {
		for user in users {
			if user.key == peerID.displayName {
				
				DispatchQueue.main.async {
					if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
						let storageManager = appDelegate.rootAssembly.storageManager
						storageManager.foundUser(withName: user.value, userID: user.key)
					}
				}
				
				delegate?.didFoundUser(withID: user.key, name: user.value)
			}
		}
	}
}

// MARK: - MCNearbyServiceAdvertiserDelegate

extension MultipeerCommunicator: MCNearbyServiceAdvertiserDelegate {
	func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
		print("Did not start advertising peer because of the error: \(error.localizedDescription)")
		delegate?.failedToStartAdvertising(error: error)
	}
	
	func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
		print("Did receive invitation from peer with peerID display name = \(peerID.displayName)")
		
		var session = findSession(withPeerID: peerID)
		if session == nil {
			session = MCSession(peer: myPeerID, securityIdentity: nil, encryptionPreference: .optional)
			session?.delegate = self
			sessions[peerID.displayName] = session
		}
		
		invitationHandler(true, session)
	}
}

// MARK: - MCNearbyServiceBrowserDelegate

extension MultipeerCommunicator: MCNearbyServiceBrowserDelegate {
	func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
		print("Lost peer with peerID display name = \(peerID.displayName)")
		sessions[peerID.displayName]?.disconnect()
	}
	
	func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
		print("Did not start browsing for peers with error: \(error.localizedDescription)")
		delegate?.failedToStartBrowsingForUsers(error: error)
	}
	
	func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
		print("Found peer with peerID display name = \(peerID.displayName)")
		
		guard peerID.displayName != myPeerID.displayName else { return }
		guard let discInfo = info else { return }
		guard let name = discInfo["userName"] else { return }
		
		var session = findSession(withPeerID: peerID)
		if session == nil {
			session = MCSession(peer: myPeerID, securityIdentity: nil, encryptionPreference: .optional)
			session?.delegate = self
			sessions[peerID.displayName] = session
			
			browser.invitePeer(peerID, to: session!, withContext: nil, timeout: 30)
		} else {
			browser.invitePeer(peerID, to: session!, withContext: nil, timeout: 30)
		}
		
		users[peerID.displayName] = name
	}
}

// MARK: - MCSessionDelegate

extension MultipeerCommunicator: MCSessionDelegate {
	func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
		print("Did receive data")
		
		let message = encoder.encode(data: data)
		
		if let createdMessage = message {
			delegate?.didReceiveMessage(message: createdMessage, fromUser: peerID.displayName, toUser: myPeerID.displayName)
		}
	}
	
	func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
		
	}
	
	func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
		
	}
	
	func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
		
	}
	
	func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
		print("Peer did change state to = \(state.rawValue)")
		switch state.rawValue {
		case 2:
			userConnected(withPeerID: peerID)
		case 0:
			delegate?.didLostUser(userID: peerID.displayName)
		default:
			break
		}
	}
}
