//
//  Encoder.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 07.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation

class Encoder {
	func encode(data: Data) -> MessageStruct? {
		let messageJson = try! JSONSerialization.jsonObject(with: data, options: [])
		
		guard let incomeMessage = messageJson as? [String: String] else { return nil }
		guard let messageText = incomeMessage["text"] else { return nil }
		guard let messageID = incomeMessage["messageID"] else { return nil }
		
		let message = MessageStruct(text: messageText, type: "Income", date: Date.init(), eventType: "TextMessage", messageID: messageID)
		
		return message
	}
}
