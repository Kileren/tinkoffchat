//
//  Parser.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 07.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation

class Decoder {
	func decode(text: String) -> MessageStruct {
		let message = MessageStruct(text: text, type: "Outcome", date: Date.init(), eventType: "TextMessage", messageID: generateMessageID())
		return message
	}
	
	func generateMessageID() -> String {
		let string = "\(arc4random_uniform(UINT32_MAX)) + \(Date.timeIntervalSinceReferenceDate) + \(arc4random_uniform(UINT32_MAX))".data(using: .utf8)?.base64EncodedString()
		return string!
	}
}
