//
//  User.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 05.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation
import CoreData
import UIKit

extension User {
	
	// MARK: - Find/Insert User
	
	static func findOrInsertUser(with userID: String, in context: NSManagedObjectContext) -> User? {
		
		guard let model = context.persistentStoreCoordinator?.managedObjectModel else {
			print("Model is not available in context!")
			assert(false)
			return nil
		}
		
		let fetchRequests = FetchRequests(model: model)
		guard let fetchRequest = fetchRequests.fetchRequestUser(withUserID: userID) else {
			return nil
		}
		
		var user: User?
		do {
			let results = try context.fetch(fetchRequest)
			assert(results.count < 2, "Multiple users with same userID")
			if let foundUser = results.first {
				user = foundUser
			}
		} catch {
			print("Failed to fetch User: \(error)")
		}
		
		if user == nil {
			user = User.insertUser(withUserID: userID, in: context)
		}
		
		return user
	}
	
	static func insertUser(withUserID userID: String, in context: NSManagedObjectContext) -> User? {
		guard let user = NSEntityDescription.insertNewObject(forEntityName: "User", into: context) as? User else {
			return nil
		}
		user.userID = userID
		return user
	}
	
	static func findAllUsersOnline(in context: NSManagedObjectContext) -> [User]? {
		guard let model = context.persistentStoreCoordinator?.managedObjectModel else { return nil }
		let fetchRequests = FetchRequests(model: model)
		guard let fetchRequest = fetchRequests.fetchRequestUsersOnline() else { return nil }
		
		do {
			let results = try context.fetch(fetchRequest)
			return results
		} catch {
			print(error.localizedDescription)
			return nil
		}
	}
	
	// MARK: - Additional Functions
	
	static func generateUserIDString() -> String {
		guard let userID = UIDevice.current.identifierForVendor?.uuidString else { return "myUserID" }
		return userID
	}
	
	static func generateCurrentUserNameString() -> String {
		return "Sergey Bogachev"
	}
}

// MARK: - Fetch Requests

extension User {
	
	static func fetchRequestUsersOnline(model: NSManagedObjectModel) -> NSFetchRequest<User>? {
		let templateName = "UsersOnline"
		guard let fetchRequest = model.fetchRequestTemplate(forName: templateName) as? NSFetchRequest<User>? else {
			assert(false, "No template with name \(templateName)")
			return nil
		}
		return fetchRequest
	}
	
	static func fetchRequestUser(model: NSManagedObjectModel) -> NSFetchRequest<User>? {
		let templateName = "User"
		guard let fetchRequest = model.fetchRequestTemplate(forName: templateName) as? NSFetchRequest<User>? else {
			assert(false, "No template with name \(templateName)")
			return nil
		}
		return fetchRequest
	}
	
	static func fetchRequestUser(withUserID userID: String, model: NSManagedObjectModel) -> NSFetchRequest<User>? {
		let templateName = "UserWithUserID"
		let variables = ["userID" : userID]
		
		guard let fetchRequest = model.fetchRequestFromTemplate(withName: templateName, substitutionVariables: variables) as? NSFetchRequest<User> else {
			assert(false, "No template with name \(templateName)")
			return nil
		}
		return fetchRequest
	}
}
