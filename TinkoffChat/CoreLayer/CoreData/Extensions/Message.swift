//
//  Message.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 10.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation
import CoreData

extension Message {
	
	static func insertMessage(withID id: String, in context: NSManagedObjectContext) -> Message? {
		guard let message = NSEntityDescription.insertNewObject(forEntityName: "Message", into: context) as? Message else {
			return nil
		}
		message.messageID = id
		return message
	}
	
	static func findMessage(withID id: String, in context: NSManagedObjectContext) -> Message? {
		
		guard let model = context.persistentStoreCoordinator?.managedObjectModel else { return nil }
		let fetchRequests = FetchRequests(model: model)
		guard let fetchRequest = fetchRequests.fetchRequestMessageWithID(id: id) else { return nil }
		
		var message: Message?
		do {
			let results = try context.fetch(fetchRequest)
			assert(results.count < 2, "Multiple messages with same messageID")
			if let foundMessage = results.first {
				message = foundMessage
			}
		} catch {
			print("Failed to fetch message: \(error)")
		}
		return message
		
	}
}

// MARK: - Fetch Requests

extension Message {
	
	static func fetchRequestMessagesForConversationID(id: String, model: NSManagedObjectModel) -> NSFetchRequest<Message>? {
		let templateName = "MessagesForConversationID"
		let variables = ["conversationID": id]
		
		guard let fetchRequest = model.fetchRequestFromTemplate(withName: templateName, substitutionVariables: variables) as? NSFetchRequest<Message> else {
			assert(false, "No template with name \(templateName)")
			return nil
		}
		return fetchRequest
	}
	
	static func fetchRequestMessageWithID(id: String, model: NSManagedObjectModel) -> NSFetchRequest<Message>? {
		let templateName = "MessageWithID"
		let variables = ["messageID": id]
		
		guard let fetchRequest = model.fetchRequestFromTemplate(withName: templateName, substitutionVariables: variables) as? NSFetchRequest<Message> else {
			assert(false, "No template with name \(templateName)")
			return nil
		}
		return fetchRequest
	}
}
