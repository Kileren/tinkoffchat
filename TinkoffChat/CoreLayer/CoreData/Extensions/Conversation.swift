//
//  Conversation.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 10.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation
import CoreData

extension Conversation {
	
	// MARK: - Find/Insert Conversation
	
	static func findOrInsertConversation(withID id: String, in context: NSManagedObjectContext) -> Conversation? {
		let fetchRequest = NSFetchRequest<Conversation>(entityName: "Conversation")
		let predicate = NSPredicate(format: "conversationID == %@", id)
		let sortDescriptor = NSSortDescriptor(key: "conversationID", ascending: true)
		fetchRequest.predicate = predicate
		fetchRequest.sortDescriptors = [sortDescriptor]
		
		var conversation: Conversation?
		do {
			let results = try context.fetch(fetchRequest)
			assert(results.count < 2, "Multiple conversations with one conversationID")
			if let foundConversation = results.first {
				conversation = foundConversation
			}
		} catch {
			print("Cant fetch conversation: \(error)")
		}
		
		if conversation == nil {
			conversation = Conversation.insertConversation(withID: id, in: context)
		}
		return conversation
	}
	
	static func insertConversation(withID id: String, in context: NSManagedObjectContext) -> Conversation? {
		if let conversation = NSEntityDescription.insertNewObject(forEntityName: "Conversation", into: context) as? Conversation {
			conversation.conversationID = id
			conversation.hasUnreadMessage = false
			conversation.isOnline = true
			return conversation
		}
		return nil
	}
}

// MARK: - Fetch Requests

extension Conversation {
	static func fetchRequestsConversationWithID(id: String, model: NSManagedObjectModel) -> NSFetchRequest<Conversation>? {
		let templateName = "ConversationWithID"
		let variables = ["conversationID": id]
		
		guard let fetchRequest = model.fetchRequestFromTemplate(withName: templateName, substitutionVariables: variables) as? NSFetchRequest<Conversation> else {
			assert(false, "No template with name \(templateName)")
			return nil
		}
		
		return fetchRequest
	}
}
