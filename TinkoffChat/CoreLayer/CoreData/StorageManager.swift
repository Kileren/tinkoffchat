//
//  StorageManager.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 05.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation
import CoreData
import UIKit

protocol IProfileStorageManager: class {
	func saveProfileInfo(info: ProfileInfo)
	func retrieveProfileInfo() -> ProfileInfo?
}

protocol IUserStorageManager: class {
	func foundUser(withName name: String, userID: String)
	func deleteUser(user: User, in receivedContext: NSManagedObjectContext)
	func changeUserToOffline(withUserID userID: String)
	func changeAllUsersToOffline()
}

protocol IMessageStorageManager: class {
	func newMessage(withText text: String, messageID: String, type: String, from senderUserID: String, to receiverUserID: String)
	func deleteMessage(withID id: String)
	func changeLastMessageForConversation(withID id: String, messageID: String)
	func messagesInConversationHasBeenRead(conversationID: String)
}

class StorageManager: NSObject {
	
	// MARK: - Stack
	
	private var _coreDataStack: CoreDataStack?
	var coreDataStack: CoreDataStack? {
		get {
			if _coreDataStack == nil {
				_coreDataStack = CoreDataStack()
			}
			
			return _coreDataStack
		}
	}
}

// MARK: - IProfileStorageManager

extension StorageManager: IProfileStorageManager {
	
	func saveProfileInfo(info: ProfileInfo) {
		guard let saveContext = self.coreDataStack?.saveContext else {
			print("Did not find saveContext")
			return
		}
		saveContext.perform {
			guard let appUser = AppUser.findOrInsertAppUser(in: saveContext) else {
				print("Did not find appUser")
				return
			}
			
			appUser.name = info.name
			appUser.aboutInfo = info.aboutInfo
			if let imageData = UIImagePNGRepresentation(info.image) {
				appUser.avatar = imageData
			} else { print("Can't convert image to Data") }
			
			self.coreDataStack?.performSave(context: saveContext, completionHandler: { } )
		}
	}
	
	func retrieveProfileInfo() -> ProfileInfo? {
		var profileInfo = ProfileInfo(image: UIImage(named: "Avatar")!,
									  name: "My name",
									  aboutInfo: "Info about me")
		guard let context = self.coreDataStack?.mainContext else {
			print("Can't get context")
			return nil
		}
		guard let appUser = AppUser.findOrInsertAppUser(in: context) else {
			print("Can't get app user")
			return nil
		}
		
		if let name = appUser.name { profileInfo.name = name }
		if let aboutInfo = appUser.aboutInfo { profileInfo.aboutInfo = aboutInfo }
		if let avatarData = appUser.avatar, let image = UIImage(data: avatarData) { profileInfo.image = image }
		return profileInfo
	}
}

// MARK: - IUserStorageManager

extension StorageManager: IUserStorageManager {
	
	func foundUser(withName name: String, userID: String) {
		
		guard let context = coreDataStack?.saveContext else { return }
		context.perform {
			guard let user = User.findOrInsertUser(with: userID, in: context), let conversation = Conversation.findOrInsertConversation(withID: userID, in: context) else {
				return
			}
			
			user.name = name
			user.isOnline = true
			
			if user.conversation == nil {
				user.conversation = conversation
			}
			user.conversation?.isOnline = true
			
			self.coreDataStack?.performSave(context: context, completionHandler: { } )
		}
	}
	
	func deleteUser(user: User, in receivedContext: NSManagedObjectContext) {
		let context = receivedContext
		context.perform {
			guard let conversation = user.conversation else {
				print("Can't get user's conversation")
				return
			}
			context.delete(conversation)
			context.delete(user)
			self.coreDataStack?.performSave(context: context, completionHandler: nil)
		}
	}
	
	func changeUserToOffline(withUserID userID: String) {
		guard let context = coreDataStack?.saveContext else { return }
		context.perform {
			guard let user = User.findOrInsertUser(with: userID, in: context) else {
				print("Can't find user with such userID")
				return
			}
			
			guard user.conversation?.lastMessage != nil else {
				self.deleteUser(user: user, in: context)
				return
			}
			
			user.isOnline = false
			user.conversation?.isOnline = false
			self.coreDataStack?.performSave(context: context, completionHandler: { })
		}
	}
	
	func changeAllUsersToOffline() {
		guard let context = coreDataStack?.saveContext else { return }
		context.perform {
			guard let usersOnline = User.findAllUsersOnline(in: context) else { return }
			for user in usersOnline {
				guard user.conversation?.lastMessage != nil else {
					self.deleteUser(user: user, in: context)
					continue
				}
				user.isOnline = false
				user.conversation?.isOnline = false
			}
			
			self.coreDataStack?.performSave(context: context, completionHandler: nil)
		}
	}
}

// MARK: - IMessageStorageManager

extension StorageManager: IMessageStorageManager {
	
	func newMessage(withText text: String, messageID: String, type: String, from senderUserID: String, to receiverUserID: String) {
		guard let context = coreDataStack?.saveContext else { return }
		context.perform {
			guard let appUser = AppUser.findOrInsertAppUser(in: context), let currentUser = appUser.currentUser else { return }
			
			let userID = currentUser.userID == senderUserID ? receiverUserID : senderUserID
			
			guard let conversation = Conversation.findOrInsertConversation(withID: userID, in: context),
				let message = Message.insertMessage(withID: messageID, in: context) else { return }
			
			message.text = text
			message.eventType = "TextMessage"
			message.type = type
			message.date = Date.init()
			message.conversationID = userID
			message.lastMessageForConversation = conversation
			conversation.hasUnreadMessage = true
			
			self.coreDataStack?.performSave(context: context, completionHandler: { } )
		}
	}
	
	func deleteMessage(withID id: String) {
		guard let context = coreDataStack?.saveContext else { return }
		context.perform {
			guard let message = Message.findMessage(withID: id, in: context) else { return }
			context.delete(message)
			self.coreDataStack?.performSave(context: context, completionHandler: nil)
		}
	}
	
	func changeLastMessageForConversation(withID id: String, messageID: String) {
		guard let context = coreDataStack?.saveContext else { return }
		context.perform {
			guard let conversation = Conversation.findOrInsertConversation(withID: id, in: context),
				let message = Message.findMessage(withID: messageID, in: context) else { return }
			conversation.lastMessage = message
			self.coreDataStack?.performSave(context: context, completionHandler: nil)
		}
	}
	
	func messagesInConversationHasBeenRead(conversationID: String) {
		guard let context = coreDataStack?.saveContext else { return }
		context.perform {
			guard let conversation = Conversation.findOrInsertConversation(withID: conversationID, in: context) else { return }
			conversation.hasUnreadMessage = false
			self.coreDataStack?.performSave(context: context, completionHandler: nil)
		}
	}
}
