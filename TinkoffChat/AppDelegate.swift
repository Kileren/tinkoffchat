//
//  AppDelegate.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 20.09.17.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
	
	let rootAssembly = RootAssembly()

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		
		return true
	}

	func applicationWillResignActive(_ application: UIApplication) {
		rootAssembly.storageManager.changeAllUsersToOffline()
		printState(withMethodCalled: "applicationWillResignActive", previousState: "active", currentState: "inactive")
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
		printState(withMethodCalled: "applicationDidEnterBackground", previousState: "inactive", currentState: "background")
	}

	func applicationWillEnterForeground(_ application: UIApplication) {
		printState(withMethodCalled: "applicationWillEnterForeground", previousState: "background", currentState: "inactive")
	}

	func applicationDidBecomeActive(_ application: UIApplication) {
		printState(withMethodCalled: "applicationDidBecomeActive", previousState: "inactive", currentState: "active")
	}

	func applicationWillTerminate(_ application: UIApplication) {
		printState(withMethodCalled: "applicationWillTerminate", previousState: "background", currentState: "not running")
	}
	
	func printState(withMethodCalled nameOfTheMethod: String, previousState: String, currentState: String) {
		print("Application moved from \(previousState) to \(currentState) state: \(nameOfTheMethod)")
	}
}
