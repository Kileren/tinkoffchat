//
//  Extensions.swift
//  TinkoffChat
//
//  Created by  Sergey Bogachev on 14.11.2017.
//  Copyright © 2017  Sergey Bogachev. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
	func scrollToLastCell(animated: Bool, afterNewMessage: Bool, section: Int) {
		var scrollNumber = 1
		if afterNewMessage { scrollNumber = 2 }
		if self.numberOfRows(inSection: section) > 1 {
			let indexPath = IndexPath(row: self.numberOfRows(inSection: section) - scrollNumber, section: 0)
			self.scrollToRow(at: indexPath, at: .bottom, animated: animated)
		}
		if afterNewMessage {
			self.scrollToLastCell(animated: true, afterNewMessage: false, section: section)
		}
	}
}

extension UILabel {
	public func changeTextColor(text: String? = nil, color: UIColor?, duration: CFTimeInterval?) {
		UIView.transition(with: self, duration: duration ?? 0.5, options: .transitionCrossDissolve, animations: {
			self.text = text ?? self.text
			self.textColor = color
		}, completion: nil)
	}
}
